<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
  <body>
    <div class="container">
      <?php
        include "nav.php";
      ?>
    </div>
      <div>
        <header style="background-color:#ffffff;background-image:url('assets/img/banner/sambutan.jpg' );background-repeat:no-repeat;background-size:cover;min-height:400px">
      </header>
      </div>
      <!--  -->
  <div class="sec-header vertical-align-bottom">
    <div class="container ">
      <div class="row">
        <div class="col-12"><h1>Sambutan</h1></div>
      </div>
    </div>
  </div>

  <div class="container mt-5">
    <div class="row">
      <div class="col-3"></div>
      <div class="col-9">
        <p><span style="color: #808080;">Puji dan syukur marilah kita panjatkan kehadirat Tuhan YME, yang atas perkenan-NYA maka website RSPI Sulianti Saroso ini dapat beroperasional (online)</span></p>
        <p><span style="color: #808080;">Website ini memuat informasi mengenai Profil RSPI Sulianti Saroso, Pelayanan Kesehatan yang ada serta berita seputar kegiatan RSPI Sulianti Saroso dalam melaksanakan kegiatan pelayanan kesehatan</span></p>
        <p><span style="color: #808080;">Website ini diharapkan dapat memberikan informasi yang lebih utuh kepada masyarakat untuk mendapatkan Pelayanan Kesehatan yang sesuai dengan kebutuhannya</span></p>
        <p><span style="color: #808080;">Kami menyadari, bahwa website ini masih jauh dari sempurna, sehingga kritik dan saran yang membangun tentunya sangat kami harapkan dari masyarakat sehingga RSPI dapat memberikan pelayanan kesehatan yang lebih baik lagi</span></p>
        <p><span style="color: #ff6600;"><strong>Terima Kasih</strong></span></p>
      </div>
    </div>
  </div>

    
  <?php
    include "foot.php";
  ?>
</body>
</html>