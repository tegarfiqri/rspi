<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
  <body>
    <div class="container">
      <?php
        include "nav.php";
      ?>
    </div>
      <div>
        <header style="background-color:#ffffff;background-image:url('assets/img/banner/layanan-unggulan4.jpg' );background-repeat:no-repeat;background-size:cover;min-height:400px">
      </header>
      </div>
      <!--  -->
  <div class="sec-header vertical-align-bottom">
    <div class="container ">
      <div class="row">
        <div class="col-12"><h1>Profil</h1></div>
      </div>
    </div>
  </div>

  <div class="container mt-5">
    <div class="row">
      <div class="col-3"></div>
      <div class="col-9">
<!--  -->
                <p><span style="color: #808080;">Sejarah RSPI Sulianti Saroso dimulai dengan keberadaan Station Karantina di Pulau Onrust Kuiper, Kepulauan Seribu, Jakarta.</span></p>
        <p><span style="color: #ff6600;"><strong>Tahun 1958,&nbsp;</strong></span><span style="color: #808080;">Station Karantina dipindahkan ke Tanjung Priok. Fungsi utama waktu itu adalah untuk menampung penderita cacar yang berasal dari Jakarta dan sekitarnya.</span></p>
        <p><strong><span style="color: #ff6600;">Tahun 1978,</span>&nbsp;</strong><span style="color: #808080;">Dengan dinyatakannya Indonesia bebas cacar pada tahun 1972, maka berdasarkan Kepmenkes RI No. 148/Menkes/SK/78 tertanggal 28 April 1978, Station Karantina&nbsp; berubah menjadi RS Karantina yang berada di bawah Direktorat Jenderal P4M, Depkes RI. dengan tugas pokok dan fungsi menyelenggarakan pelayanan pengobatan, perawatan, karantina dan isolasi serta pengelolaan penyakit menular tertentu sesuai dengan Peraturan Perundang-undangan yang berlaku.</span></p>
        <p><span style="color: #ff6600;"><strong>Tanggal 17 Juni 1992,&nbsp;</strong></span><span style="color: #808080;">Dilakukan peletakan batu pertama pembangunan RSPI Sulianti Saroso oleh Menteri&nbsp; Kesehatan RI, Dr. Adhyatma MPH, disaksikan oleh Duta Besar Jepang, Michihiko Kunihiro di lokasi sekarang ini melalui bantuan Hibah Pemerintahan Jepang.</span></p>
        <p><strong><span style="color: #ff6600;">Tanggal 20 Jan 1994,</span>&nbsp;</strong><span style="color: #808080;">Berdasarkan Kepmenkes RI No. 55/Menkes/SK/1994 tentang Organisasi dan Tata Kerja, RSPI Sulianti Saroso merupakan unit organik Depkes yang bertanggung jawab langsung ke Ditjen PPM dan PLP, Depkes RI. Tujuan jangka panjang RSPI-SS adalah sebagai pusat rujukan nasional penyakit menular dan penyakit infeksi lainnya.</span></p>
        <p><strong><span style="color: #ff6600;">Tanggal 1 April 1994,</span>&nbsp;</strong><span style="color: #808080;">RSPI Sulianti Saroso&nbsp;diresmikan olah Menteri Kesehatan, Prof. Dr. Sujudi, disaksikan oleh Dubes Jepang, dan Gubernur DKI Jakarta pada waktu itu Letnan Jenderal (Purn) Soerjadi Soedirdja.</span></p>
        <p><strong><span style="color: #ff6600;">Tahun 1996,</span>&nbsp;</strong><span style="color: #808080;">Berdasarkan Kepmenkes No 113/Menkes/SK/II/96, RSPI Sulianti Saroso menjadi Rumah Sakit tipe B non-Pendidikan.</span></p>
        <p><strong><span style="color: #ff6600;">Tanggal 13 Jan 2005,</span>&nbsp;</strong><span style="color: #808080;">Berdasarkan Kepmenkes No. 66/Menkes/SK/I/2005, RSPI Sulianti Saroso menjadi RS Vertikal, Tipe B Pendidikan, Eselon II B.</span></p>
        <p><strong><span style="color: #ff6600;">Tanggal 21 Juni 2007,</span>&nbsp;</strong><span style="color: #808080;">Berdasarkan Kepmenkeu No. 270/KMK.05/2007 dan Kepmenkes No. 756/Menkes/SK/VI/2007, RSPI Sulianti Saroso menjadi Badan Layanan Umum (BLU).</span></p>
        <p><strong><span style="color: #ff6600;">Tanggal 11 Maret 2008,</span>&nbsp;</strong><span style="color: #808080;">Berdasarkan Kepmenkes No. 247/Menkes/PER/III/2008, RSPI Sulianti Saroso menjadi RS Tipe B Pendidikan, Eselon II A.</span></p>
        <p><strong><span style="color: #ff6600;">Tanggal 25 Nov 2009,</span>&nbsp;</strong><span style="color: #808080;">Berdasarkan Kepmenkes No. 1138/Menkes/SK/XI/2009, RSPI Sulianti Saroso merupakan Pusat Kajian dan Rujukan Nasional Penyakit Infeksi.</span></p>
        <p><strong><span style="color: #ff6600;">Tanggal 25 Okt 2011,</span>&nbsp;</strong><span style="color: #808080;">Berdasarkan Permenkes No. 2073/Menkes/PER/X/2011, RSPI Sulianti Saroso merupakan Unit Pelaksana Teknis (UPT) di bawah Ditjen BUK.</span></p>
<!--  -->
      </div>
    </div>
  </div>

    
  <?php
    include "foot.php";
  ?>
</body>
</html>