<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
  <body>
    <div class="container">
      <?php
        include "nav.php";
      ?>
    </div>
      <div>
        <header style="background-color:#ffffff;background-image:url('assets/img/banner/jadual3.jpg' );background-repeat:no-repeat;background-size:cover;min-height:400px">
      </header>
      </div>
      <!--  -->
  <div class="sec-header vertical-align-bottom">
    <div class="container ">
      <div class="row">
        <div class="col-12"><h1>Jadwal Dokter</h1></div>
      </div>
    </div>
  </div>

  <div class="container mt-5">
    <div class="row">
      <div class="col-3"></div>
      <div class="col-9">
        <!--  -->

        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>GAWAT DARURAT</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">Dokter Umum</span></td>
        <td><span style="color: #999999;">Setiap hari – 24 Jam</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK ORTOPEDI</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. William Chandra, SpOT</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK PENYAKIT DALAM</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Iman Firmansyah, SpPD, FINASIM</span></td>
        <td><span style="color: #999999;">Rabu dan&nbsp; Selasa</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Dina Oktavia, SpPD</span></td>
        <td><span style="color: #999999;">Senin, Selasa dan Kamis</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK TB DOTS</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Titi Sundari, SpP</span></td>
        <td><span style="color: #999999;">Kamis</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Adria Rusli, SpP</span></td>
        <td><span style="color: #999999;">Jum’at</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Rosa Marlina, SpP</span></td>
        <td><span style="color: #999999;">Selasa</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Pompini Agustina Sitompul, SpP</span></td>
        <td><span style="color: #999999;">Rabu</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Ida Bagus Sila Wiweka, SpP</span></td>
        <td><span style="color: #999999;">Senin</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK BEDAH SYARAF</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. AM Ilham Patu, SpBS</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK BEDAH UMUM &amp; ONKOLOGI</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Afrimal Syafaruddin, SpB (K) Onk</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK THT</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Mutia Budiati, SpTHT KL</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK NEUROLOGI</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Natan Payangan, SpS</span></td>
        <td><span style="color: #999999;">Kamis</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Waria, SpS</span></td>
        <td><span style="color: #999999;">Senin dan Rabu</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Maria Laurensia Tampubulon, SpS</span></td>
        <td><span style="color: #999999;">Selasa</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK ANAK</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Ernie Setyawati, SpA, MKes</span></td>
        <td><span style="color: #999999;">Rabu</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Desrinawati, SpA</span></td>
        <td><span style="color: #999999;">Kamis</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Sri Sulastri, SpA</span></td>
        <td><span style="color: #999999;">Rabu</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Dedet Hidayati, SpA</span></td>
        <td><span style="color: #999999;">Jum’at dan Selasa</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Suci Romadhona,Msi Med, SpA</span></td>
        <td><span style="color: #999999;">Senin</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK OBGYN</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Roni Pumala Bangun, SpOG</span></td>
        <td><span style="color: #999999;">Rabu dan Jum’at</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK KULIT DAN KELAMIN</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Indah Handayani, SpKK</span></td>
        <td><span style="color: #999999;">Senin, Rabu dan Kamis</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Ni Luh Putu Pitawati, SpKK</span></td>
        <td><span style="color: #999999;">Selasa dan Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK MATA</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Emilia Setyaadmadja, SpM</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK GIZI</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. joyce Magdalena Santoso, MS, SpGK</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK GIGI DAN MULUT</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">drg. Titin Sumarni</span></td>
        <td><span style="color: #999999;">Senin, Selasa dan Jumat</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">drg. Adriana Nani Julifa</span></td>
        <td><span style="color: #999999;">Rabu dan kamis</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK REHABILITASI MEDIK</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Juan Suseno Haryanto, SpKFR</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">Fisioterapi</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">Psikologi (dengan Perjanjian)</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK MEDICAL CHECK UP</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Nunung Hendrawati, MKK</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK IMUNISASI</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Nunung Hendrawati, MKK</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <p><span style="color: #ff6600;"><strong>POLIKLINIK MELATI</strong></span></p>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%"><strong>DOKTER</strong></td>
        <td width="70%"><strong>HARI</strong></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Adria Rusli, SpP</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        <tr>
        <td><span style="color: #999999;">dr. Sucahyo Adi Nugroho</span></td>
        <td><span style="color: #999999;">Senin s/d Jumat</span></td>
        </tr>
        </tbody>
        </table>
        <p>&nbsp;</p>
        <!--  -->
      </div>
    </div>
  </div>

    
  <?php
    include "foot.php";
  ?>
</body>
</html>