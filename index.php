<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
  <body>
    <div class="container">
      <?php
        include "nav.php";
      ?>
    </div>
      <div>
        <header style="background-color:#ffffff;background-image:url('assets/img/banner/selamat-datang01.jpg' );background-repeat:no-repeat;background-size:cover;min-height:400px">
      </header>
      </div>
      <!--  -->
  <div class="sec-header vertical-align-bottom">
    <div class="container ">
      <div class="row">
        <div class="col-12"><h1>Selamat Datang</h1></div>
      </div>
    </div>
  </div>

  <div class="container mt-5">
    <div class="row">
      <div class="col-3"></div>
      <div class="col-9">
        <!--  -->
        <p>Selamat Datang di website&nbsp;<em><span style="color: #ff6600;"><strong> Rumah Sakit Penyakit Infeksi Prof. dr. Sulianti Saroso Jakarta…. </strong></span></em></p>
        <p>&nbsp;</p>
        <p><strong>VISI</strong></p>
        <p>Menjadi RS Rujukan Nasional dan&nbsp;Pusat<b>&nbsp;</b>Kajian Penyakit Infeksi Yang Terdepan Setingkat Asia Pasifik Tahun 2019.</p>
        <p>&nbsp;</p>
        <p><b>MISI</b><b></b></p>
        <ol>
        <li>Menyelenggarakan&nbsp; pengelolaan penyakit infeksi termasuk&nbsp;new emerging, re-emerging, dan tropical medicine&nbsp;secara paripurna dan profesional berbasis&nbsp;quality dan safety.</li>
        <li>Menyelenggarakan kajian, penelitian sesuai dengan standar ilmiah, etik, berbasis bukti dan nilai untuk pengembangan, pencegahan dan penanggulangan penyakit infeksi termasuk&nbsp;new emerging, re-emerging, dan tropical medicine.</li>
        <li>Menyelenggarakan pendidikan dan pelatihan penyakit infeksi termasuk&nbsp;new emerging, re-emerging, dan tropical medicine&nbsp;secara profesional</li>
        <li>Menyelenggarakan jejaring pelayanan, pendidikan dan penelitian di bidang penyakit infeksi termasuk&nbsp;new emerging, re-emerging, dan tropical medicine&nbsp;Nasional dan Internasional</li>
        </ol>
      </div>
    </div>
  </div>

    
  <?php
    include "foot.php";
  ?>
</body>
</html>