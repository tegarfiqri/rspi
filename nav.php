<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/jquery/jquery-ui.css">

 	<div class="row Nav vertical-align">
	    <div class="col-3">
	      <img src="assets/img/logo-gabung1.jpg" alt="RSPI Sulianti Saroso" class="normal-logo" height="90" style="padding: -7.5px 0; max-height: 90px;">
	    </div>
	    <div class="col-9">
			<ul class="nav justify-content-end">
			  <li class="nav-item">
			    <a href="index.php" class="nav-link text-warning active" href="#">Selamat Datang</a>
			  </li>
			  <li class="nav-item">
			    <a href="sambutan.php" class="nav-link disabled" href="#">Sambutan</a>
			  </li>
			  <li class="nav-item">
			    <a href="profil.php" class="nav-link disabled" href="#">Profil</a>
			  </li>
			  <li class="nav-item">
			    <a href="jadwal.php" class="nav-link disabled" href="#">Jadwl Dokter</a>
			  </li>
			  <li class="nav-item">
			    <a href="berita.php" class="nav-link disabled" href="#">Berita</a>
			  </li>
			  <li class="nav-item">
			    <a href="informasi.php" class="nav-link disabled" href="#">Informasi & Pemasaran</a>
			  </li>
			  <li class="nav-item">
			    <a href="tautan.php" class="nav-link disabled" href="#">Tautan</a>
			  </li>
			</ul>
	    </div>
  	</div>