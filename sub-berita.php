  <div class="c-berita">
    <div class="container">
      <?php
        include "nav.php";
      ?>
    </div>
      
      <!--  -->
  <div class="sec-header vertical-align-bottom">
    <div class="container ">
      <div class="row">
        <div class="col-12"><h1>Penandatanganan kontrak kinerja Tahun 2018 – RSPI Prof. Dr. Sulianti Saroso</h1></div>
      </div>
    </div>
  </div>

  <div class="container mt-5">
    <div class="row">
      <a href="#">Admin</a>
      <div class="col-2">15 Januari 2018</div>
    </div>
    <div class="row pt-3">
      <div class="media">
        <img class="mr-4" src="http://rspi-suliantisaroso.co.id/wp-content/uploads/2018/01/IMG-20180115-WA0000-300x203.jpg" alt="IMG-20180115-WA0000">
        <div class="media-body">
              Jakarta (12/1)- Pelaksanaan rencana kerja RSPI Sulianti Saroso pada tahun 2018 ini diawali dengan Penandatanganan Kontrak Kinerja Tahun 2018 yang dilakukan oleh seluruh direksi, pejabat struktural dan pejabat fungsionalnya.<br>
                <br>
            acara yang dilakukan pada hari jumat, 12 Januari 2018, pk 13.00, di Ruangan Auditorium lt.VI tersebut dihadiri lengkap oleh jajaran direksi, Ketua  Satuan Pengawas Intern, seluruh Ketua Komite, Pejabat Eselon III dan IV, Kepala Instalasi.<br>
              <br>
            Kontrak kinerja adalah dokumen yang merupakan kesepakatan antara pegawai dengan atasan langsung yang paling sedikit berisi pernyataan kesanggupan, sasaran kerja pegawai dan trajectory target yang harus dicapai dalam periode tertentu (KMK No.467/2014)<br>
              <br>
            Direktur Utama RSPI- SS, dr. Rita Rogayah, SpP (K) MARS. Dalam arahannya menyampaikan bahwa acara seperti ini memang harus diadakan pada setiap awal tahunnya serta beliau mangharapkan agar para pejabat Struktral dan Fungsional di Lingkungan RSPI Sulianti Saroso mampu memberikan  pelayanan terbaiknya bagi masyarakat dan negara serta senantiasa berupaya untuk memenuhi butir-butir janji yang disepakati dalam kontrak kinerja tersebut. Terus komitmen dan teguh dengan setiap amanah yang dibebankan serta yang terpenting, mampu mamikul amanah tersebut.<br>
              <br>
            Sebagai insan Aparatur Sipil Negara, yang ditempatkan di RSPI Prof.dr. Sulianti Saroso, kami siap bekerja dengan profesional dan sungguh-sungguh.<br>
              <br>
            Pada akhirnya, dengan Kontrak kinerja diharapkan visi dan misi dan seluruh indikator yang menjadi target RSPI Sulianti Saroso dapat tercapai dengan baik.
        </div>

      </div>
     <!--  <div class="col-12" style="align-items: top">
        <div class="mr-4 mb-4" style="width: 300px; height: 203; float: left">
          <img class="size-medium wp-image-2666 alignleft" src="http://rspi-suliantisaroso.co.id/wp-content/uploads/2018/01/IMG-20180115-WA0000-300x203.jpg" alt="IMG-20180115-WA0000">
        </div>
        <div style="float: left;">
              Jakarta (12/1)- Pelaksanaan rencana kerja RSPI Sulianti Saroso pada tahun 2018 ini diawali dengan Penandatanganan Kontrak Kinerja Tahun 2018 yang dilakukan oleh seluruh direksi, pejabat struktural dan pejabat fungsionalnya.<br>
                <br>
            acara yang dilakukan pada hari jumat, 12 Januari 2018, pk 13.00, di Ruangan Auditorium lt.VI tersebut dihadiri lengkap oleh jajaran direksi, Ketua  Satuan Pengawas Intern, seluruh Ketua Komite, Pejabat Eselon III dan IV, Kepala Instalasi.<br>
              <br>
            Kontrak kinerja adalah dokumen yang merupakan kesepakatan antara pegawai dengan atasan langsung yang paling sedikit berisi pernyataan kesanggupan, sasaran kerja pegawai dan trajectory target yang harus dicapai dalam periode tertentu (KMK No.467/2014)<br>
              <br>
            Direktur Utama RSPI- SS, dr. Rita Rogayah, SpP (K) MARS. Dalam arahannya menyampaikan bahwa acara seperti ini memang harus diadakan pada setiap awal tahunnya serta beliau mangharapkan agar para pejabat Struktral dan Fungsional di Lingkungan RSPI Sulianti Saroso mampu memberikan  pelayanan terbaiknya bagi masyarakat dan negara serta senantiasa berupaya untuk memenuhi butir-butir janji yang disepakati dalam kontrak kinerja tersebut. Terus komitmen dan teguh dengan setiap amanah yang dibebankan serta yang terpenting, mampu mamikul amanah tersebut.<br>
              <br>
            Sebagai insan Aparatur Sipil Negara, yang ditempatkan di RSPI Prof.dr. Sulianti Saroso, kami siap bekerja dengan profesional dan sungguh-sungguh.<br>
              <br>
            Pada akhirnya, dengan Kontrak kinerja diharapkan visi dan misi dan seluruh indikator yang menjadi target RSPI Sulianti Saroso dapat tercapai dengan baik.
          </div>
      </div> -->
    </div>
    <div class="row">
        <div class="p-5" style="height: 100px;"><a href="berita.php" style="vertical-align: middle"><i class="fas fa-folder"></i> Berita</a></div>
    </div>
    <hr>
    <div class="row py-5">
      <div class="col-12" align="center">
          <button class="btn btn-secondary"><i class="fab fa-facebook-f"> </i> Like</button>
          <button class="btn btn-secondary"><i class="fab fa-twitter"> </i> Tweet</button>
          <button class="btn btn-secondary"><i class="fab fa-google-plus"> </i> + 1</button>
          <button class="btn btn-secondary"><i class="fab fa-pinterest"> </i> Pin it</button>
      </div>
    </div>
  </div>
  <div class="foot-news">
    <div class="container">
      <div class="row">
        <div class="col-12 mt-4">
          <h3>Related Stories</h3>
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-12 p-3 news-list">
          <div class="title">
            <a href="#">Seminar Ilmiah “Siap Siaga Menghadapi Wabah Dipteri”</a>
          </div>
          <div class="col-2 tgl" align="center">
            20 Desember
          </div>
          <div class="content">
            Pada hari kamis tanggal 21 Desember 2017, RSPI Sulianti Saroso telah mengadakan Seminar Ilmiah “Siap Siaga Menghadapi…
          </div>
          <div class="foot">
            Poste in: <a href="berita.php">Berita</a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 p-3 news-list">
          <div class="title">
            <a href="#">Seminar Ilmiah “Siap Siaga Menghadapi Wabah Dipteri”</a>
          </div>
          <div class="col-2 tgl" align="center">
            20 Desember
          </div>
          <div class="content">
            Pada hari kamis tanggal 21 Desember 2017, RSPI Sulianti Saroso telah mengadakan Seminar Ilmiah “Siap Siaga Menghadapi…
          </div>
          <div class="foot">
            Poste in: <a href="berita.php">Berita</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    
  <?php
    include "foot.php";
?>