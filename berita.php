<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
  <body>
    <div class="container">
      <?php
        include "nav.php";
      ?>
    </div>

      <!--  -->
  <div class="sec-header vertical-align-bottom">
    <div class="container ">
      <div class="row">
        <div class="col-12"><h1>Berita</h1></div>
      </div>
    </div>
  </div>

  <div class="container mt-5 c-berita">
    <div class="row row-news">
      <div class="col-2" align="center"><font class="main-date">15</font><hr width="40%"><font class="sub-date">10'18</font></div>
      <div class="col-10 preview-news p-4">
        <!--  -->
          <a href="sub-berita.php">Penandatanganan Kontrak Kinerja Tahun 2018 - RSPI Prof.Dr.Sulianti Saroso</a>
          <p class="pt-4">Jakarta (12/1)- Pelaksanaan rencana kerja RSPI Sulianti Saroso pada tahun 2018 ini diawali dengan Penandatanganan Kontrak Kinerja Tahun 2018 yang dilakukan oleh seluruh direksi, pejabat struktural dan pejabat fungsionalnya. acara yang dilakukan pada hari jumat, 12 Januari 2018, pk 13.00, di Ruangan Auditorium lt.VI tersebut dihadiri lengkap oleh jajaran direksi, Ketua  Satuan Pengawas Intern, seluruh…</p>
        <!--  -->
      </div>
    </div>
    <div class="row row-news">
      <div class="col-2" align="center" style="background: #f5f5f6"><font class="main-date">15</font><hr width="40%"><font class="sub-date">10'18</font></div>
      <div class="col-10 preview-news p-4">
        <!--  -->
          <a href="sub-berita.php">Penandatanganan Kontrak Kinerja Tahun 2018 - RSPI Prof.Dr.Sulianti Saroso</a>
          <p class="pt-4">Jakarta (12/1)- Pelaksanaan rencana kerja RSPI Sulianti Saroso pada tahun 2018 ini diawali dengan Penandatanganan Kontrak Kinerja Tahun 2018 yang dilakukan oleh seluruh direksi, pejabat struktural dan pejabat fungsionalnya. acara yang dilakukan pada hari jumat, 12 Januari 2018, pk 13.00, di Ruangan Auditorium lt.VI tersebut dihadiri lengkap oleh jajaran direksi, Ketua  Satuan Pengawas Intern, seluruh…</p>
        <!--  -->
      </div>
    </div>
  </div>

    
  <?php
    include "foot.php";
  ?>
</body>
</html>