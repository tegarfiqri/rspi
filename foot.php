<footer class="mt-5">	
	<div class="container first-foot">
		<div class="row pt-5 px-4">
			<div class="col-3">Tentang website</div>
			<div class="col-3">Berita & Informasi terkini</div>
			<div class="col-3">Hubungi kami</div>
			<div class="col-3">Main Menu</div>
		</div>
		<div class="row pt-5 px-4">
			<div class="col-3 f-st">
				Website ini dikelola oleh Subbag Informasi dan Pemasaran, Bagian Program dan Anggaran Direktorat Keuangan, Umum & Administrasi RSPI Sulianti Saroso
			</div>
			<div class="col-3 f-nd">
				<div>
					<div>
						Seminar Ilmiah "Siap Siaga Menghadapi Wabah Dipteri"
					</div>
					<div class="pb-3">
						December 20, 2017
					</div>
				</div>
				<div>
					<div>
						Seminar Ilmiah "Siap Siaga Menghadapi Wabah Dipteri"
					</div>
					<div class="pb-3">
						December 20, 2017
					</div>
				</div>
			</div>
			<div class="col-3 f-rd">
				RSPI SULIANTI SAROSO<br>
				Jalan SUnter Permai Raya, Jakarta utara 14340 <br>
				Indonesia<br>
				<br>
				Telp : (021) 6506559 (Hunting) Ex. 1120<br>
				Fax : (021) 6401411<br>
				E-mail: informasi.sulianti@gmail.com<br>
				<br>
				Get direction on the map
			</div>
			<div class="col-3 f-th">
				<ul>
					<li><a href="http://rspi-suliantisaroso.co.id/sulianti">Selamat Datang</a></li>
					<li><a href="http://rspi-suliantisaroso.co.id/sulianti/a-homepage-section/">Sambutan</a></li>
					<li><a href="http://rspi-suliantisaroso.co.id/sulianti/about/">Profil</a></li>
					<li><a href="http://rspi-suliantisaroso.co.id/sulianti/jadual-dokter/">Jadual Dokter</a></li>
					<li><a href="http://rspi-suliantisaroso.co.id/sulianti/category/berita/">Berita</a></li>
					<li><a href="http://rspi-suliantisaroso.co.id/sulianti/category/informasi-pemasaran/">Informasi &amp; Pemasaran</a></li>
					<li><a href="http://rspi-suliantisaroso.co.id/sulianti/tautan-terkait/">Tautan</a></li>
				</ul>
			</div>
		</div>
	</div>
	<hr>
	<div class="container second-foot">
		<div class="row py-2">
			<div class="col-2">© 2017 - by Zamasco</div>
			<div class="col-8" align="center"><button class="btn btn-light" id="btn-top" style="height: 35px;"><i class="fas fa-arrow-up"></i></button></div>
		</div>
	</div>
</footer>
<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-ui/jquery-ui.js"></script>
<script >
	$("#btn-top").click(function () {
	   //1 second of animation time
	   //html works for FFX but not Chrome
	   //body works for Chrome but not FFX
	   //This strange selector seems to work universally
	   $("html, body").animate({scrollTop: 0}, 1000);
	});
</script>